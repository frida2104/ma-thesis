# knowledge_base.py uses this file (scraper.py) to scrape information 
# from the FAQ and Encyclopedia of the Nutrition Centre (Voedingscentrum). 


import unidecode
import requests
from bs4 import BeautifulSoup
from difflib import get_close_matches, SequenceMatcher



def format_query(question, delimiter):
	'''Removes punctuation marks, replaces accented characters and lowercases text.'''
	punctuation = ",.;:?()'\""
	for p in punctuation:
		question = question.replace(p, '').lower()
	question = unidecode.unidecode(question)
	formatted_question = delimiter.join(question.split())
	return formatted_question


def format_answer(answer):
	'''Removes whitespace from a scraped answer.'''
	return answer.strip().replace("\r\n", " ").replace("\n", " ")


def get_page(url):
	'''Returns the html page of a given URL.'''
	data = requests.get(url)
	page = data.text
	soup = BeautifulSoup(page, "lxml")
	return soup


def scrape_faq(page):
	'''Scrapes an answer from the FAQ of the Nutrition Centre.'''
	text = page.find_all("div", class_="freecontent")

	if len(text) < 2:
		return "fail"

	answer_text = text[1].find()
	if answer_text == None:
		return "fail"

	try:
		first_text = answer_text.previousSibling
	except:
		first_text = " "

	if answer_text.text == "":
		answer_text = answer_text.find_next()
	if first_text.isspace() == True:
		answer = answer_text.text
	else:
		answer = first_text
	
	if "." not in answer:
		return "fail"
	else:
		return format_answer(answer)


def scrape_encyclopedia(page):
	'''Scrapes an answer from the Encyclopedia of the Nutrition Centre.'''
	text = page.find("div", id="ctl00_ctl00_main_ctl00_pC")
	if text == None or text.text == "":
		try:
			text = text.find_next()
		except:
			return "fail"

	answer = text.text
	return format_answer(answer)

