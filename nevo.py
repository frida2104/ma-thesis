# This file contains functions regarding the Dutch Food Composition Table (NEVO) 
# and is used by actions.py


import pandas as pd
import unidecode
from difflib import get_close_matches



def pd_data16():
	'''Adds modified food names to the NEVO food database
	   and returns it as a pandas dataframe.
	'''
	with open("nevo16.csv", "r") as f:
		df = pd.read_csv(f, sep=";")
		df = df[df.Productgroep_oms != "Preparaten"]
		df["Product_naam"] = food_names(df)
	return df


def food_names(df):
	'''	Returns a list of generated food names 
		based on the product description in the NEVO.
	'''
	foods = df["Product_omschrijving"].tolist()
	food_names = []
	for food in foods:
		food = food.lower().split()
		food_name = food[0]

		if len(food) > 1:
			if food[1].endswith("-") == True:
				food_name = food[1].rstrip("-") + food_name
				
		chars = "'-()"
		for char in chars:
			food_name = food_name.replace(char, "")

		food_names.append(unidecode.unidecode(food_name))

	return food_names


def find_food(input_food):
	'''Searches for a food product in the NEVO (exact match); 
	   returns the food name, else returns an empty string.
	'''
	df = pd_data16()
	food_list = df["Product_naam"].to_list()
	if input_food in food_list:
		return input_food
	else:
		return ""


def find_close_food(input_food):
	'''Searches for a food in the NEVO (close match);
	   returns the most matching food, else returns an empty string.
	'''
	df = pd_data16()
	food_list = df["Product_naam"].to_list()
	try:
		found_food = get_close_matches(input_food, food_list, n=1, cutoff=0.6)
		return found_food[0]
	except:
		return ""

		
def get_health_tag(input_food):
	'''Returns the health tag (healthy (G) or unhealthy (O) of a food in the NEVO.'''
	df = pd_data16()
	tag = df.loc[df.Product_naam == input_food, "Gezond_tag"].values[0]
	return tag


def get_food_group(input_food):
	'''Returns the food group of a food in the NEVO,
	   else returns an empty string
	'''
	df = pd_data16()
	try:
		food_group = df.loc[df.Product_naam == input_food, "Productgroep_oms"].values[0]
	except:
		return ""

	food_groups = {"aardappelen": ["aardappelen", "N"],
	"Alcoholische en niet-alcoholische dranken": ["dranken", "N"],
	"Brood": ["brood", "N"],
	"Diversen": ["", "N"],
	"Eieren": ["eieren", "G"],
	"Fruit": ["fruit", "G"],
	"Gebak en koek": ["", "O"],
	"Graanproducten en bindmiddelen": ["granen en graanproducten", "N"],
	"Groenten": ["groente", "G"],
	"Hartig broodbeleg": ["", "N"],
	"Kaas": ["kaas", "O"],
	"Kruiden en specerijen": ["kruiden en specerijen", "G"],
	"Melk en melkproducten": ["melk en melkproducten", "N"],
	"Noten, zaden en snacks": ["", "N"],
	"Peulvruchten": ["peulvruchten", "G"],
	"Samengestelde gerechten": ["", "N"],
	"Soepen": ["", "N"],
	"Sojaproducten en vegetarische producten": ["soja en sojaolie", "N"],
	"Suiker, snoep, zoet beleg en zoete sauzen": ["suiker en zoetigheid", "O"],
	"Vetten, oliën en hartige sauzen": ["vetten", "O"],
	"Vis": ["vis", "G"],
	"Vlees, vleeswaren en gevogelte": ["vlees", "N"]}

	return food_groups[food_group][0]


def food_info(food, nutrient):
	'''Returns the content (float number) of a nutrient of a food.'''
	df = pd_data16()
	info = df.loc[df.Product_naam == food, nutrient].values[0]
	if "sp" in str(info) or str(info) == "":
		info = 0
	return float(info)


def properties(food, health_tag):
	'''Creates a list of remarkable nutrients in a food
	   based on the nutrient contents and health tag of a food.
	'''
	if health_tag == "G":
		nutrients = [("eiwit", "_02002", 15.0), \
					 ("enkelvoudig onverzadigd vet", "_03006", 10.0), \
					 ("meervoudig onverzadigd vet", "_03008", 10.0), \
					 ("vezel", "_06001", 3.0)]

	elif health_tag == "O":
		nutrients = [("energie", "_01001", 250.0), \
					 ("vet", "_03001", 12.0), \
					 ("verzadigd vet", "_03004", 15.0), \
					 ("transvet", "_03136", 1.0), \
					 ("cholesterol", "_04001", 200.0), \
					 ("suiker", "_05002", 8.0), \
					 ("alcohol", "_08001", 1.0), \
					 ("zout", "_09006", 120.0)]
	
	features = []
	for nutrient in nutrients:
		if food_info(food, nutrient[1]) > nutrient[2] and food != nutrient[0]:
			features.append(nutrient[0])

	return features

		
def check_number(food):
	'''Checks if a food is singular or plural;
	   returns a list with the corresponding verbs.
	'''
	if food.endswith("en") == True and food.endswith("oen") == False:
		number = "plural"
	elif food.endswith("'s") == True:
		number = "plural"
	elif food.endswith("es") == True and food.endswith("oes") == False and food.endswith("ees") == False:
		number = "plural"
	elif food[-1] == "s" and food[-2] not in "aeuioj":
		number = "plural"
	elif food[-1] == "s" and food[-2] in "aeuio" and food[-3] in "aeuio":
		number = "singular"
	else:
		number = "singular"
	
	if number == "plural":
		return ["zijn", "zit", "behoren"]
	else:
		return ["is", "zit", "behoort"]

