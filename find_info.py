# This file searches for information in the knowledge base (FAQ and Encyclopedia)
# and is used by actions.py


import pandas as pd
import unidecode
from difflib import get_close_matches, SequenceMatcher
from scraper import format_query



def find_close_match(query, topics, source):
	'''Finds close matches of a query and topics and questions;
	   returns the (close) match with the highest similarity score and the score itself. 
	'''
	close_matches = get_close_matches(format_query(query, " "), topics, n=3, cutoff=0.6)
	if len(close_matches) == 0:
		return "", 0

	ratios = [SequenceMatcher(None, query, match).ratio() for match in close_matches]
	max_ratio = max(ratios)
	index = ratios.index(max_ratio)
	correct_match = close_matches[index]
	return correct_match, max_ratio


def find_match(query, source):
	'''Finds an exact or close match of a query and a topic or question
	   returns the match and the similarity score. 
	'''
	df = pd.read_pickle(source)
	topic_list = df["topic"].tolist()
	if query in topic_list:
		match = query
		ratio = 1
		return match, ratio

	if source == "faq":
		match, ratio = find_close_match(query, topic_list, source)
		return match, ratio

	elif source == "encyclopedia":
		synonyms = df["synonyms"].tolist()
		for synonym_set in synonyms:
			if query in synonym_set:
				index = synonyms.index(synonym_set)
				match = topic_list[index]
				ratio = 1
				return match, ratio

		found_match, ratio = find_close_match(query, topic_list, source)
		return found_match, ratio
		
	return "", 0


def get_answer(query, intent, source):
	'''Finds an answer in the FAQ or encyclopedia based on 
	   the intent and the match with the query. 
	   Returns the match, answer and similarity score. 
	'''
	df = pd.read_pickle(source)

	match, ratio = find_match(query, source)
	if match == "":
		return match, [], ratio
	
	answer = df.loc[df.topic == match, intent].values[0]
	return match, answer, ratio


def check_fail(entities, match, answer, ratio):
	'''Checks if the ratio (similarity score) is high enough. 
	   Returns True if the answer is insufficient; 
	   returns False if the answer is sufficient. 
	'''
	if answer == []:
		return True

	if ratio > 0.85:
		return False

	if len(entities) > 0:
		if any(entity not in match for entity in entities):
			return True

	if ratio > 0.75:
		return False
	
	return True


def find_info(query, topic, intent):
	'''Finds the match, answer and the source in which the answer is found.
	'''
	intents = { "vc_question": "advice", \
				"what_is_question":"what_is", \
				"healthy_question": "healthy", \
				"advice_question": "advice", \
				"advice_question_noentity": "advice"}

	if intent not in intents:
		intent = "advice"
	else:
		intent = intents[intent]

	source = "faq"
	entities = []
	if topic != None:
		entities += list(set(topic))

	match, answer, ratio = get_answer(query, "answer", source)

	if check_fail(entities, match, answer, ratio) == True:
		if entities != []:
			query = " ".join(entities)
			match, answer, ratio = get_answer(query, "answer", source)

			if check_fail(entities, match, answer, ratio) == True:
				source = "encyclopedia"
				match, answer, ratio = get_answer(query, intent, source)
						
				if check_fail(entities, match, answer, ratio) == True:
					return source, "", []
		else:
			return source, "", []
	
	return source, match, answer

