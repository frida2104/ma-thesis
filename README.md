﻿﻿﻿﻿﻿﻿# Dutch Rasa Chatbot for Nutrition Related Questions

This repository contains the files for the chatbot developed with Rasa for my MA Thesis for Information Science. 


## Requirements

Python 3.6 or higher is required. 

Install Rasa (NLU and Core):
```
pip install rasa=1.1.3
```

Install the spaCy Dutch language model for Rasa NLU:

```
python3 -m spacy download nl
```

Installation guide for Rasa:
https://rasa.com/docs/rasa/user-guide/installation/#


## Create the Knowledge Base

Download FAQ and Encyclopedia from the Netherlands Nutrition Centre website: 

```
wget -r -np www.voedingscentrum.nl/encyclopedie/*.aspx
wget -r -np www.voedingscentrum.nl/nl/service/vraag-en-antwoord/*/*.aspx
```

Create FAQ:

```
python3 knowledge_base.py faq
```

Create Encyclopedia:

```
python3 knowledge_base.py encyclopedia
```


## Train and run the chatbot

Train the chatbot:
```
rasa train
```

Run the chatbot in the terminal with action server:
```
rasa run actions
rasa shell
``` 
