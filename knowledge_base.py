# This file creates the knowledge base of the FAQ or Encyclopedia 
# and saves it to a pickle file. 
#
# Commands:
# python3 knowledge_base.py faq
# python3 knowledge_base.py encyclopedia


import os
import sys
import pandas as pd
import numpy as np
from bs4 import BeautifulSoup
from nltk.tokenize import sent_tokenize, word_tokenize
from scraper import *



def save_df(df, source):
	'''Pickles a pandas dataframe given the FAQ or Encyclopedia.'''
	df.to_pickle(source)


def find_title(page):
	'''Returns the title (lowercased and stripped) of an html page.'''
	title = page.find("title").text
	title = title[:-18].lower().strip()
	return title


def get_synonyms(title):
	'''Generates a list of synonyms of a topic, 
	   based on the title of an html page.
	'''
	title_list = title.replace(")", "").replace("(", "$").replace(" en ", "$").replace(",", "$").split("$")
	synonyms = [format_query(name, " ") for name in title_list]
	return synonyms


def find_sents(page, aspect):
	'''Scrapes the sentences of the given aspect on a topic page'''
	aspects = {"what_is":"blok1", "healthy":"blok3", "advice":"blok7"}
	blok = aspects[aspect]

	text = page.find("a", {"name":blok})
	if text == None and blok == "blok1":
		text = page.find("div", id="ctl00_ctl00_main_ctl00_pC")
	if text == None:
		return []

	info_text = ""
	while len(info_text) == 0:
		text = text.find_next(["p", "div"])
		info_text = text.text.strip()

	scraped_sents = sent_tokenize(info_text)
	sents = [sent.replace("\r\n", " ").replace("\n", " ") \
			 for sent in scraped_sents if sent.startswith("Lees meer") == False]
	return sents


def sort_sents(title, intro_sents, terms, all_terms):
	'''Returns filtered sentences (of a topic) 
	   containing certain keywords belonging to an aspect.
	'''
	filtered_sents = []

	for sent in sent_tokenize(intro_sents):
		if any(x in sent.lower() for x in all_terms[terms]):
			filtered_sents.append(sent)

	return filtered_sents


def find_info(page, title):
	'''Creates a dictionary of a topic containing (formatted) sentences of three aspects.'''
	info = {}
	intro_sents = scrape_encyclopedia(page)

	aspects = {"what_is":["is een"], \
				 "healthy":["gezond", "effecten", "risico"], \
				 "advice":["advi", "gram", "schijf van vijf"]}

	for aspect, keywords in aspects.items():
		sents = find_sents(page, aspect)

		if intro_sents == "fail":
			filtered_intro_sents = []
		else:
			filtered_intro_sents = sort_sents(title, intro_sents, aspect, aspects)

		for sent in filtered_intro_sents:
			if sent not in sents:
				sents.append(sent)

		info[aspect] = sents

	return info


def get_paths(source):
	'''Generates the path names to the directories or the FAQ categories.'''
	if source == "faq":
		path = "www.voedingscentrum.nl/nl/service/vraag-en-antwoord/"
		categories = ["gezonde-voeding-en-voedingsstoffen/", \
					  "aandoeningen/", "afvallen-en-gewicht/", \
					  "zwanger-en-baby/", \
					  "kinderen-en-jongeren/", \
					  "eten-kopen-en-keurmerken/", \
					  "veilig-eten-en-e-nummers/", \
					  "koken-en-bewaren/", \
					  "vragen-aan-het-voedingscentrum/"]
		paths = [path + cat for cat in categories]

	elif source == "encyclopedia":
		paths = ["www.voedingscentrum.nl/encyclopedie/"]

	return paths


def scrape(source):
	'''Creates a pandas dataframe containing information 
	   from the FAQ or Encyclopedia of the Voedingscentrum.
	'''
	topics, answers, synonyms, what_is, advice, healthy = [], [], [], [], [], []

	paths = get_paths(source)
	for path in paths:
		for filename in os.listdir(path):
			if filename.endswith(".aspx"):
				with open(path + filename, encoding='utf-8') as f:
					page = BeautifulSoup(f.read(), "lxml")

					if source == "faq":
						answer = scrape_faq(page)

						if answer != "fail":
							topics.append(format_query(find_title(page), " "))
							answers.append(sent_tokenize(answer.strip().replace("\r\n", " ")))

					elif source == "encyclopedia":
						title = find_title(page)
						topics.append(title)
						synonyms.append(get_synonyms(title))
						
						info = find_info(page, title)
						what_is.append(info["what_is"])
						healthy.append(info["healthy"])
						advice.append(info["advice"])

	if source == "faq":
		df = pd.DataFrame({"topic":topics, "answer":answers})
	elif source == "encyclopedia":
		df = pd.DataFrame({"topic":topics, "synonyms":synonyms, "what_is":what_is, "healthy":healthy, "advice":advice,})

	return save_df(df, source)


scrape(sys.argv[1])

