# This file contains custom actions used by Rasa Core (dialogue manager). 


import logging
logger = logging.getLogger(__name__)

import rasa.core
from rasa_sdk import Action
from rasa_sdk.events import *

import random
from difflib import SequenceMatcher

from nevo import *
from find_info import *



class ActionResetSlots(Action):
	'''Resets slots and memory.'''
	def name(self):
		return "action_reset"

	def run(self, dispatcher, tracker, domain):
		intent = tracker.get_slot("intent")
		topic = tracker.get_slot("topic")
		if topic != None:
			topic_intent = [t + ";" + intent.split("_")[0] for t in topic]
			memory = tracker.get_slot("memory")
			if memory == None:
				memory = []
			memory += topic_intent
			return [Restarted(), \
					SlotSet("intent", intent), \
					SlotSet("memory", memory), \
					FollowupAction("action_listen")]

		return [Restarted(), \
				SlotSet("intent", intent), \
				FollowupAction("action_listen")]


class ActionSetQuestionIntent(Action):
	'''Sets a slot with nutrition related question (and its intent) asked by user, 
	   and redirects to the scrape question action. 
	'''
	def name(self):
		return "action_set_question_intent"

	def run(self, dispatcher, tracker, domain):
		question = tracker.latest_message['text']
		intent =  tracker.latest_message['intent'].get('name')
		if intent.endswith("question") == True:
			return [SlotSet("last_question", question), SlotSet("intent", intent)]

		memory = tracker.get_slot("memory")
		topic = memory[-1].split(";")[0]
		if intent.startswith("advice"):
			return [SlotSet("last_question", ""), SlotSet("topic", [topic]), SlotSet("intent", intent)]

		return [SlotSet("last_question", ""), SlotSet("topic", [topic])]


class ActionAnswerQuestion(Action):
	'''Searches for the answer in FAQ or encyclopedia of the Nutrition Centre. 
	'''
	def name(self):
		return "action_answer_question"

	def run(self, dispatcher, tracker, domain):
		food_group, food_group_messages = [], []

		question = tracker.get_slot("last_question")
		if question == None:
			question = ""
		input_topic = tracker.get_slot("topic")
		intent = tracker.get_slot("intent")

		source, match, answer = find_info(question, input_topic, intent)

		if answer == []:  # find input food(s) + corresponding food groups in NEVO, 
						  # then try to scrape again with food groups
			if input_topic == [] or input_topic == None:
				return [FollowupAction("action_fallback")]

			food_group = [get_food_group(food) for food in input_topic]
			if "" in food_group:
				return [FollowupAction("action_find_food")]

			for i in range(len(food_group)):
				question = question.replace(input_topic[i], food_group[i])

			source, match, answer = find_info(question, food_group, intent)

			if answer == []:
				return [FollowupAction("action_find_food")]

			for i in range(len(food_group)):
				verbs = check_number(input_topic[i])
				food_group_messages.append(input_topic[i].capitalize() + \
										   " " + verbs[2] + \
										   " tot de voedingsgroep " + \
										   food_group[i] + ".")
				input_topic[i] = food_group[i]

		if source == "encyclopedia":  # clarify encyclopedia topic if no exact match
			queries = [question] + input_topic
			if not any(x == match for x in queries):
				return [SlotSet("found_topic", match), \
						SlotSet("answer", answer), \
						FollowupAction("action_clarify")]
		
		if food_group_messages != []:
			for message in food_group_messages:
				dispatcher.utter_message(message)

		return [SlotSet("topic", input_topic), SlotSet("answer", answer)]


class ActionGiveAnswer(Action):
	'''Returns the (final) answer.'''
	def name(self):
		return "action_give_answer"

	def run(self, dispatcher, tracker, domain):
		answer = tracker.get_slot("answer")
		if answer == None:
			return [FollowupAction("action_fallback")] 
		memory = tracker.get_slot("memory")
		if memory == None:
			memory = []
		topic = tracker.get_slot("topic")
		if topic != None:
			intent = tracker.get_slot("intent").split("_")[0]
			topic_intent = topic[0].replace(" ", "_") + ";" + intent
			count = memory.count(topic_intent)
			if not count >= len(answer):
				answer_sent = answer[count]
			else:
				answer_sent = answer[random.randrange(len(answer))]
		else:
			answer_sent = answer[random.randrange(len(answer))]

		dispatcher.utter_message(answer_sent)
		return []


class ActionFindFood(Action):
	'''Find the food (or misspelled food) from the question in the NEVO. 
	'''
	def name(self):
		return "action_find_food"

	def run(self, dispatcher, tracker, domain):
		input_topic = tracker.get_slot("topic")
		matches = []
		food = []
		found_food = []
		for i in range(len(input_topic)):
			found = find_food(input_topic[i])
			if found == "":
				found = find_close_food(input_topic[i])
				if found != "":
					food.append(input_topic[i])
					found_food.append(found)
			else:
				food.append(input_topic[i])
				found_food.append(input_topic[i])

		if food != []:
			if food == found_food:
				return [SlotSet("food", food), \
						FollowupAction("action_food_healthy")]
			else:
				return [SlotSet("food", food), \
						SlotSet("found_food", found_food), \
						FollowupAction("action_clarify")]

		return [FollowupAction("action_topic_unknown")]


class ActionClarify(Action):
	'''Ask for confirmation to correct topic from the encyclopedia or food name from the NEVO. 
	'''
	def name(self):
		return "action_clarify"

	def run(self, dispatcher, tracker, domain):
		found_food = tracker.get_slot("found_food")
		if found_food == None or found_food == []:
			found_topic = tracker.get_slot("found_topic")
			dispatcher.utter_template("utter_confirm_topic", tracker, topic=found_topic)
		else:
			dispatcher.utter_template("utter_confirm_food", tracker, food=found_food[0])

		return [FollowupAction("action_listen")]


class ActionReplaceTopic(Action):
	'''Replaces food for corrected food in original question. 
	'''
	def name(self):
		return "action_replace_topic"

	def run(self, dispatcher, tracker, domain):
		question = tracker.get_slot("last_question")
		input_topic = tracker.get_slot("topic")
		found_topic = tracker.get_slot("found_topic")
		if found_topic == None:
			found_topic = tracker.get_slot("found_food")[0]
		question = question.replace(input_topic[0], found_topic)
		input_topic[0] = found_topic

		return [SlotSet("last_question", question), \
				SlotSet("topic", input_topic), \
				SlotSet("found_topic", None)]


class ActionReplaceFood(Action):
	'''Replaces food for corrected food in original question. 
	'''
	def name(self):
		return "action_replace_food"

	def run(self, dispatcher, tracker, domain):
		question = tracker.get_slot("last_question")
		input_topic = tracker.get_slot("topic")
		food = tracker.get_slot("food")
		found_food = tracker.get_slot("found_food")

		question = question.replace(food[0], found_food[0])
		topic = [x if x != food[0] else found_food[0] for x in input_topic]
		del food[0]
		del found_food[0]

		if food == found_food:
			return [SlotSet("last_question", question), \
					SlotSet("topic", topic), \
					FollowupAction("action_answer_question")]
		
		return [SlotSet("last_question", question), \
				SlotSet("topic", topic), \
				SlotSet("food", food), \
				SlotSet("found_food", found_food), \
				FollowupAction("action_clarify")]


class ActionTopicUnknown(Action):
	'''Tell user the input food does not occur in the NEVO. 
	'''
	def name(self):
		return "action_topic_unknown"

	def run(self, dispatcher, tracker, domain):
		input_item = tracker.get_slot("topic")
		if input_item == None:
			input_item = []
		found_food = tracker.get_slot("found_food")
		if found_food == None:
			found_food = []
		if input_item == []:
			input_item = tracker.get_slot("topic")

		else:
			different_food = [food for food in input_item if food not in found_food]  # list of corrected foods
			if len(different_food) > 1:
				unknown = ", ".join(different_food[:-1]) + " en " + different_food[-1]
			else:
				unknown = different_food[0]

		dispatcher.utter_template("utter_unknown", tracker, unknown=unknown)
		return [FollowupAction("action_reset")]


class ActionFoodHealthy(Action):
	'''Finds health tag in NEVO (healthy (G) or unhealthy (O)) + info about nutrient contents
	   and returns the appropriate response for each food. 
	'''
	def name(self):
		return "action_food_healthy"

	def run(self, dispatcher, tracker, domain):
		foods = tracker.get_slot("food")
		for food in foods:
			verbs = check_number(food)
			health_tag = get_health_tag(food)

			if health_tag == "N":
				dispatcher.utter_template("utter_neutral", tracker, food=food.capitalize(), verb=verbs[0])
			else:
				if health_tag == "G":
					dispatcher.utter_template("utter_healthy", tracker, food=food.capitalize(), verb=verbs[0])
				elif health_tag == "O":
					dispatcher.utter_template("utter_unhealthy", tracker, food=food.capitalize(), verb=verbs[0])

				nutrients = properties(food, health_tag)
				if len(nutrients) > 0:
					if len(nutrients) > 1:
						explanation = ", ".join(nutrients[:-1]) + " en " + nutrients[-1]
					else:
						explanation = nutrients[0]
					dispatcher.utter_template("utter_explanation", tracker, food=food, verb=verbs[1], nutrients=explanation)

		dispatcher.utter_template("utter_vary", tracker)
		return [FollowupAction("action_reset")]


class ActionFallback(Action):
	'''Executes the fallback action and goes back to the previous state of the dialogue. 
	'''
	def name(self):
		return "action_fallback"

	def run(self, dispatcher, tracker, domain):
		dispatcher.utter_template("utter_default", tracker, silent_fail=True)
		return [UserUtteranceReverted(), FollowupAction("action_reset")]

