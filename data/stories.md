## story_greet
* greet
 - utter_greet
 - utter_question
 - utter_knowledge

## story_goodbye
* goodbye
 - utter_goodbye

## story_thanks
* thanks
 - utter_thanks


## story_nutrition_question
* vc_question{"topic":["topic"]} OR what_is_question{"topic":["topic"]} OR advice_question{"topic":["topic"]} OR healthy_question{"topic":["topic"]}
 - action_set_question_intent
 - slot{"last_question":"question"}
 - slot{"intent":"intent"}
 - action_answer_question
 - slot{"topic":["topic"]}
 - slot{"answer":["answer"]}
 - action_give_answer
 - action_reset
 - slot{"intent":"intent"}
 - slot{"memory":["food_intent"]}

## story_nutrition_question_more
* vc_question{"topic":["topic"]} OR what_is_question{"topic":["topic"]} OR advice_question{"topic":["topic"]} OR healthy_question{"topic":["topic"]}
 - action_set_question_intent
 - slot{"last_question":"question"}
 - slot{"intent":"intent"}
 - action_answer_question
 - slot{"topic":["topic"]}
 - slot{"answer":["answer"]}
 - action_give_answer
 - action_reset
 - slot{"intent":"intent"}
 - slot{"memory":["food_intent"]}
* ask_more_info
 - action_set_question_intent
 - slot{"topic":["topic"]}
 - slot{"last_question":"question"}
 - action_answer_question
 - action_give_answer
 - action_reset
 - slot{"intent":"intent"}
 - slot{"memory":["food_intent"]}
 
## story_nutrition_question_advice
* vc_question{"topic":["topic"]} OR what_is_question{"topic":["topic"]} OR healthy_question{"topic":["topic"]}
 - action_set_question_intent
 - slot{"last_question":"question"}
 - slot{"intent":"intent"}
 - action_answer_question
 - slot{"topic":["topic"]}
 - slot{"answer":["answer"]}
 - action_give_answer
 - action_reset
 - slot{"intent":"intent"}
 - slot{"memory":["food_intent"]}
* advice_question_noentity
 - action_set_question_intent
 - slot{"topic":["topic"]}
 - slot{"last_question":"question"}
 - slot{"intent":"advice_question_noentity"}
 - action_answer_question
 - action_give_answer
 - action_reset
 - slot{"intent":"intent"}
 - slot{"memory":["food_intent"]}
 
## story_nutrition_question_repeat
* vc_question{"topic":["topic"]} OR what_is_question{"topic":["topic"]} OR advice_question{"topic":["topic"]} OR healthy_question{"topic":["topic"]}
 - action_set_question_intent
 - slot{"last_question":"question"}
 - slot{"intent":"intent"}
 - action_answer_question
 - slot{"topic":["topic"]}
 - slot{"answer":["answer"]}
 - action_give_answer
 - action_reset
 - slot{"intent":"intent"}
 - slot{"memory":["food_intent"]}
* repeat
 - action_answer_question
 - action_give_answer
 - action_reset
 - slot{"intent":"intent"}
 - slot{"memory":["food_intent"]}

## story_question with topic_clarification
* vc_question{"topic":["topic"]} OR what_is_question{"topic":["topic"]} OR advice_question{"topic":["topic"]} OR healthy_question{"topic":["topic"]}
 - action_set_question_intent
 - slot{"last_question":"question"}
 - slot{"intent":"intent"}
 - action_answer_question
 - slot{"found_topic":"found_topic"}
 - slot{"answer":["answer"]}
 - action_clarify
> check_topic_question

## story_affirm_topic_question
> check_topic_question
* affirm
 - action_replace_topic
 - slot{"last_question":"question"}
 - slot{"topic":["topic"]}
 - slot{"found_topic":"None"}
 - action_answer_question
 - slot{"topic":["topic"]}
 - slot{"answer":["answer"]}
 - action_give_answer
 - action_reset
 - slot{"intent":"intent"}
 - slot{"memory":["food_intent"]}

## story_deny_topic_question
> check_topic_question
* deny
 - action_topic_unknown


## story_question with food
* vc_question{"topic":["topic"]} OR what_is_question{"topic":["topic"]} OR advice_question{"topic":["topic"]} OR healthy_question{"topic":["topic"]}
 - action_set_question_intent
 - slot{"last_question":"question"}
 - slot{"intent":"intent"}
 - action_answer_question
 - slot{"food":["food"]}
 - action_food_healthy
 - action_reset
 - slot{"intent":"intent"}
 - slot{"memory":["food_intent"]}

## story_question with food_clarification
* vc_question{"topic":["topic"]} OR what_is_question{"topic":["topic"]} OR advice_question{"topic":["topic"]} OR healthy_question{"topic":["topic"]}
 - action_set_question_intent
 - slot{"last_question":"question"}
 - slot{"intent":"intent"}
 - action_answer_question
 - slot{"food":["food"]}
 - slot{"found_food":["found_food"]}
 - action_clarify
> check_food_question

## story_affirm_food_question
> check_food_question
* affirm
 - action_replace_food
 - slot{"last_question":"question"}
 - slot{"topic":["topic"]}
 - action_answer_question
 - slot{"topic":["topic"]}
 - slot{"answer":["answer"]}
 - action_give_answer
 - action_reset
 - slot{"intent":"intent"}
 - slot{"memory":["food_intent"]}

## story_affirm_food_question
> check_food_question
* affirm
 - action_replace_food
 - slot{"last_question":"question"}
 - slot{"topic":["topic"]}
 - slot{"food":["food"]}
 - slot{"found_food":["found_food"]}
 - action_replace_food

## story_deny_food_question
> check_food_question
* deny
 - action_topic_unknown


## story_other
* deny OR affirm
 - action_listen

