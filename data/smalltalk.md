## intent:affirm
- absoluut
- ja
- ja dat klopt
- ja hoor
- ja klopt
- ja natuurlijk
- jawel
- jazeker
- klopt
- maar natuurlijk
- natuurlijk
- tuurlijk
- wel
- welles
- yes
- zeker
- zekers
- zeker wel

## intent:deny
- echt niet
- nee
- nee echt niet
- nee hoor
- neen
- nee niet
- nee zeker niet
- niet
- nietes
- no
- nope


## intent:greet
- goedemiddag
- goedemorgen
- goedenavond
- goedendag
- goeiedag
- goeiemiddag
- goeiemorgen
- goeienavond
- goeiendag
- haai
- hai
- hallo
- hallo daar
- he
- hey
- heya
- hoi
- hooi
- yo

## intent:goodbye
- dag
- doeg
- doei
- hoi
- ik ben weg
- ik ga ervandoor
- later
- laters
- tot de volgende keer
- tot later
- tot straks
- tot ziens
- welterusten
- zie je later


## intent:thanks
- dank
- bedankt
- dankje
- dankjewel
- danku
- dankuwel
- heel erg bedankt
- hartstikke bedankt
- bedankt hoor
- bedankt daarvoor
- bedankt voor je hulp
- bedankt voor je antwoord
- oke bedankt
- ok dankje
- thanks
- thanx


## intent:name
- ik [Lola](name)
- ik ben [Richard](name)
- k ben [Ben](name)
- kben [Charlie](name)
- 'k ben [Philip](name)
- [Kim](name) ben ik
- ik heet [Lucas](name)
- naam is [Tom](name)
- me naam is [Amy](name)
- mijn naam is [Frida](name)
- m'n naam is [Jan](name)
- mn naam is [Piet](name)
- naam [Leo](name)
- [Sophie](name) is m'n naam
- [Stan](name) is mijn naam
- [Susan](name) is naam
- noem mij [Linda](name)
- noem me [Lily](name)
- noem me maar [Susan](name)
- ik noem mezelf [David](name)
- sommigen noemen mij [Julia](name)
- iedereen noemt me [Laura](name)
- ik word [Bert](name) genoemd
- je mag me [Sam](name) noemen
